<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_sms extends CI_Model
{
	var $table = 'history_sms';
    var $column_order = array('id_history_sms', 'id_client', 'id_user', 'client', 'username', 'created_date', 'destination_phone'); //set column field database for datatable orderable
    var $column_search = array('id_history_sms', 'id_client', 'id_user', 'client', 'username', 'created_date', 'destination_phone'); //set column field database for datatable searchable 
    var $order = array('id_history_sms' => 'desc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('pt_indocoll', true);
    }

    private function _get_datatables_query($month = NULL, $years = NULL)
    {
        $this->db->from($this->table);        
        if ($month) {
            $this->db->where('MONTH(created_date)', $month);
        }
        if ($years) {
            $this->db->where('YEAR(created_date)', $years);
        }

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($this->input->post('search')['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like('LOWER(' . $item . ')', strtolower($this->input->post('search')['value']));
                } else {
                    $this->db->or_like('LOWER(' . $item . ')', strtolower($this->input->post('search')['value']));
                }
                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($month = "jancok", $years)
    {
        $this->_get_datatables_query($month, $years);
        if ($_POST['length'] != -1)
             $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered($month = NULL, $years = NULL)
    {
        $this->_get_datatables_query($month, $years);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($month = NULL, $years = NULL)
    {
        $this->db->from($this->table);
        if ($month) {
            $this->db->where('MONTH(created_date)', $month);
        }
        if ($years) {
            $this->db->where('YEAR(created_date)', $years);
        }
        return $this->db->count_all_results();
    }

    public function get_all_get_data($month = NULL, $years = NULL)
    {
        $this->db->from($this->table);
        if($mouth){
            $this->db->where('MONTH(created_date)', $mouth);
        }
        if($years){
            $this->db->where('YEAR(created_date)', $years);
        }
        $query = $this->db->get();
        // var_dump($this->db->last_query()); die;
        return $query->result();
        
    }


}

?>