<?php

class M_skiptrace extends CI_Model
{
    var $table = 'skip_trash';
    var $column_order = array('id_skip_trash', 'fk_id_client', 'instagram', 'facebook', 'no_phone_nasabah', 'alamat_nasabah', 'email_nasabah'); //set column field database for datatable orderable
    var $column_search = array('id_skip_trash', 'fk_id_client', 'instagram', 'facebook', 'no_phone_nasabah', 'alamat_nasabah', 'email_nasabah'); //set column field database for datatable searchable 
    var $order = array('id_skip_trash' => 'desc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_skip_trace()
    {
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) // loop kolom 
        {
            // if ($this->input->post('search')['value']) // jika datatable mengirim POST untuk search
            if ($this->input->post('search')) {
                if ($i === 0) // looping pertama
                {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('search')['value']);
                } else {
                    $this->db->or_like($item, $this->input->post('search')['value']);
                }
                if (count($this->column_search) - 1 == $i) //looping terakhir
                    $this->db->group_end();
            }
            $i++;
        }

        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function get_datatables_skip_trace()
    {
        $this->get_skip_trace();
        if ($this->input->post('length') != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        // $query->result(); echo $this->db->last_query(); die;
        return $query->result();
    }

    public function count_filtered_skip_trace()
    {
        $this->get_skip_trace();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all_skip_trace()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_all($offset = null, $limit = null, $order_by = 'id_skip_trash', $sortorder = 'desc', $param = array(), $total = false, $cache_time = 0)
    {

        if (@$cache_time != 0) {
            $this->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'file'));
            $key = "faq#" . $offset . "#" . $limit . "#" . $order_by . "#" . $sortorder . '#' . serialize($param);
            $get_result = $this->cache->get($key);
            if (!empty($get_result))
                return $get_result;
        }

        $db = $this->db;
        $select = !empty($param['select']) ? $param['select'] : '*';
        $where_clause = !empty($param['where']) ? $param['where'] : '';
        $like_clause = !empty($param['like']) ? $param['like'] : '';
        $db->select($select)
            ->from($this->table);
        (!empty($where_clause)) ? $db->where($where_clause) : '';
        (!empty($like_clause)) ? $db->or_like($like_clause) : '';
        ($limit != null) ? $db->limit($limit, $offset) : '';

        $db->order_by($order_by, $sortorder);
        $q = $db->get();
        $data['results'] = $q->result();
        // $data['results_array'] = $q->result_array();

        if ($total == true) {
            $db->select('count(*) as total_results');
            (!empty($where_clause)) ? $db->where($where_clause) : '';
            (!empty($like_clause)) ? $db->or_like($like_clause) : '';
            $q = $db->get($this->table);
            $row = $q->row();
            $data['total_results'] = $row->total_results;
        }

        if (@$cache_time != 0) {
            $this->cache->save($key, $data, $cache_time);
        }

        return $data;
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
        return true;
    }

    function update($id, $data = array())
    {
        $this->db->where('id_skip_trash', $id);
        $this->db->update($this->table, $data);
    }

    function updateStatus($id_laporan, $data)
    {
        $this->db->where('id_skip_trash', $id_laporan);
        $this->db->update($this->table, $data);
    }

    function delete($id)
    {
        $this->db->where('id_skip_trash', $id)->delete($this->table);
        return $this;
    }
}
