<?php

class M_inventorybarang extends CI_Model
{
    var $table = 'inventory_barang';
    var $column_order = array('id_users', 'img_barcode', 'type_barang', 'nama_barang','type_barang','qty','keterangan','status'); //set column field database for datatable orderable
    var $column_search = array('id_users', 'img_barcode', 'type_barang', 'nama_barang', 'type_barang','qty','keterangan','status'); //set column field database for datatable searchable 
    var $order = array('id_barang' => 'desc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('pt_indocoll', true);
    }

    private function _get_datatables_query()
    {
        // var_dump($this->column_search); die;
        $this->db->from($this->table);
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($this->input->post('search')['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like('LOWER(' . $item . ')', strtolower($this->input->post('search')['value']));
                } else {
                    $this->db->or_like('LOWER(' . $item . ')', strtolower($this->input->post('search')['value']));
                }
                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_all($offset = null, $limit = null, $order_by = 'id_barang', $sortorder = 'desc', $param = array(), $total = false, $cache_time = 0)
    {

        if (@$cache_time != 0) {
            $this->load->driver('cache', array('adapter' => 'memcached', 'backup' => 'file'));
            $key = "faq#" . $offset . "#" . $limit . "#" . $order_by . "#" . $sortorder . '#' . serialize($param);
            $get_result = $this->cache->get($key);
            if (!empty($get_result))
                return $get_result;
        }

        $db = $this->db;
        $select = !empty($param['select']) ? $param['select'] : '*';
        $where_clause = !empty($param['where']) ? $param['where'] : '';
        $like_clause = !empty($param['like']) ? $param['like'] : '';
        $db->select($select)
            ->from($this->table);
        (!empty($where_clause)) ? $db->where($where_clause) : '';
        (!empty($like_clause)) ? $db->or_like($like_clause) : '';
        ($limit != null) ? $db->limit($limit, $offset) : '';

        $db->order_by($order_by, $sortorder);
        $q = $db->get();
        $data['results'] = $q->result();
        // $data['results_array'] = $q->result_array();

        if ($total == true) {
            $db->select('count(*) as total_results');
            (!empty($where_clause)) ? $db->where($where_clause) : '';
            (!empty($like_clause)) ? $db->or_like($like_clause) : '';
            $q = $db->get($this->table);
            $row = $q->row();
            $data['total_results'] = $row->total_results;
        }

        if (@$cache_time != 0) {
            $this->cache->save($key, $data, $cache_time);
        }

        return $data;
    }

    function insert($data)
    {
        $this->db->insert($this->table, $data);
        return true;
    }

    function update($id, $data)
    {
        $this->db->where('id_barang', $id);
        $this->db->update($this->table, $data);
    }

    function updateStatus($id_laporan, $data)
    {
        $this->db->where('id_laporan', $id_laporan);
        $this->db->update($this->table, $data);
    }

    function delete($id)
    {
        $this->db->where('id_barang', $id)->delete($this->table);
    }
}
