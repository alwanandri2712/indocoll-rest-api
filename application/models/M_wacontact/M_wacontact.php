<?php

class M_wacontact extends CI_Model
{
    public $table = 'history_blast';

    public function __construct()
    {
        return parent::__construct();
        $this->load->library('form_validation');
    }

    public function empty_response()
    {
        $response['status'] = 200;
        $response['error'] = true;
        $response['message'] = 'field tidak boleh kosong';
        return $response;
    }


    public function get_count() {
        return $this->db->count_all($this->table);
    }

    public function get_wacontact($limit, $start) {
        $this->db->limit($limit, $start);
        $query = $this->db->get($this->table);

        return $query->result();
    }

    public function post_wacontact($data = array())
    {   
        return $this->db->insert($this->table, $data);
    }
}
