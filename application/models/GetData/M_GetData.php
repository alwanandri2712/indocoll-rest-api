<?php 

if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_GetData extends CI_Model
{
	var $table         = 'history_get';
	var $column_order  = array('id_history_get', 'fk_id_user', 'nm_client', 'nm_file', 'created_date', 'ttl_hits', 'created_by');  //set column field database for datatable orderable
	var $column_search = array('id_history_get', 'fk_id_user', 'nm_client', 'nm_file', 'created_date', 'ttl_hits', 'created_by');  //set column field database for datatable searchable 
	var $order         = array('id_history_get' => 'desc');                                                                        // default order 

    public function __construct()
    {
        parent::__construct();
        $this->db = $this->load->database('get_data', true);
    }

    private function _get_datatables_query($client = "", $month = "")
    {
        // var_dump($this->column_search); die;
        $this->db->from($this->table);
        // var_dump($client); die;
        if ($client) {
            $this->db->where('nm_client', $client);
        }
        if ($month) {
            $this->db->where('MONTH(created_date)', $month);
        }
        $i = 0;
        foreach ($this->column_search as $item) {
            if ($this->input->post('search')['value']) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like('LOWER(' . $item . ')', strtolower($this->input->post('search')['value']));
                } else {
                    $this->db->or_like('LOWER(' . $item . ')', strtolower($this->input->post('search')['value']));
                }
                if (count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
        // jika datatable mengirim POST untuk order
        if ($this->input->post('order')) {
            $this->db->order_by($this->column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables($client = "", $month = "")
    {
        $this->_get_datatables_query($client, $month);
        if ($_POST['length'] != -1)
             $this->db->limit($this->input->post('length'), $this->input->post('start'));
        $query = $this->db->get();
        // var_dump($this->db->last_query()); die;
        return $query->result();
    }

    function count_filtered($client = "", $month = "")
    {
        $this->_get_datatables_query($client, $month);
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all($client = "", $month = "")
    {
        $this->db->from($this->table);
        if ($client) {
            $this->db->where('nm_client', $client);
        }
        if ($month) {
            $this->db->where('MONTH(created_date)', $month);
        }
        return $this->db->count_all_results();
    }

    public function get_all_get_data($client = "", $mouth = "")
    {
        $this->db->from($this->table);
        if ($client) {
            $this->db->where('nm_client', $client);
        }
        if($mouth){
            $this->db->where('MONTH(created_date)', $mouth);
        }
        $query = $this->db->get();
        // var_dump($this->db->last_query()); die;
        return $query->result();
        
    }
    
    public function get_settings(){
        $this->db->from('setting');
        $this->db->where('setting', 'limit_saldo_server');
        $query = $this->db->get();
        return $query->result();
    }

    public function get_settings_where($where){
        $this->db->from('setting');
        $this->db->where('setting', $where);
        $query = $this->db->get();
        return $query->result();
    }

    public function get_ttl_hits_today($client = "", $date){
        date_default_timezone_set('Asia/Jakarta');

        // $this->db->from($client);
        // $this->db->select('SUM(value) as ttl_hits');
        // $this->db->where('setting', 'limit_day');
        // if ($client) {
        //     // $this->db->where('nm_client', $client);
        //     // $this->db->where('created_date', date('Y-m-d'));
        // }
        // $query = $this->db->get();
        // return $query->result();
        $this->db->from($this->table);
        $this->db->select('SUM(ttl_hits) as ttl_hits');
        if ($client) {
            $this->db->where('nm_client', $client);
            $this->db->where('created_date', $date);
        }else{
            $this->db->where('created_date', $date);   
        }
        $query = $this->db->get();
        // var_dump($this->db->last_query()); die;
        return $query->result();
    }
}

?>