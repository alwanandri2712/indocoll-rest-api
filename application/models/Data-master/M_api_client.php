<?php

    class M_api_client extends CI_Model
    {
        public function empty_response()
        {
            $response['status'] = 200;
            $response['error'] = true;
            $response['message'] = 'field tidak boleh kosong';
            return $response;
        }

        public function all_client()
        {
            $client = $this->db->get('client')->result();
            $response['meesage'] = 'berhasil get data client';
            $response['status'] = 200;
            $response['error'] = false;
            $response['data'] = $client;
            return $response; 
        }

        public function add_client($id_client, $nama_aplikasi, $nama_client, $ttl_target, $grade, $dpd, $fee, $budget_insentif, $budget_insentif_tl, $img_client, $is_active, $is_delete, $lock_data, $lock_data_time, $created_by, $created_date, $update_by, $update_date)
        {
            if (empty($id_client) || empty($nama_aplikasi) || empty($nama_client) || empty($ttl_target) || empty($created_by) || empty($update_by) || empty($update_by)) {
            return $this->empty_response();
            } else {
                $data = array(
                    'id_client' => $id_client,
                    'nama_aplikasi' => $nama_aplikasi,
                    'nama_client' => $nama_client,
                    'ttl_target' => $ttl_target,
                    'grade' => $grade,
                    'dpd' => $dpd,
                    'fee' => $fee,
                    'budget_insentif' => $budget_insentif,
                    'budget_insentif_tl' => $budget_insentif_tl,
                    'img_client' => $img_client,
                    'is_active' => $is_active,
                    'is_delete' => $is_delete,
                    'lock_data' => $lock_data,
                    'lock_data_time' => $lock_data_time,
                    'created_by' => $created_by,
                    'created_date' => $created_date,
                    'update_by' => $update_by,
                    'update_date' => $update_date
                );
                $insert = $this->db->insert('client', $data);
                
                if ($insert) {
                    $response['status']=200;
                    $response['error']=false;
                    $response['message']='Data client ditambahkan.';
                    return $response;
                } else {
                    $response['status']=502;
                    $response['error']=true;
                    $response['message']='Data client gagal ditambahkan.';
                    return $response;
                }
            }
        }

        public function update_client($id_client, $nama_aplikasi, $nama_client, $ttl_target, $grade, $dpd, $fee, $budget_insentif, $budget_insentif_tl, $img_client, $is_active, $is_delete, $lock_data, $lock_data_time, $created_by, $created_date, $update_by, $update_date)
        {
            if (empty($id_client) || empty($nama_aplikasi) || empty($nama_client) || empty($ttl_target) || empty($created_by) || empty($update_by) || empty($update_by))
            {
                return $this->empty_response();
            } else {

                $where = array(
                    'id_client' => $id_client
                );

                $data = array(
                    'id_client' => $id_client,
                    'nama_aplikasi' => $nama_aplikasi,
                    'nama_client' => $nama_client,
                    'ttl_target' => $ttl_target,
                    'grade' => $grade,
                    'dpd' => $dpd,
                    'fee' => $fee,
                    'budget_insentif' => $budget_insentif,
                    'budget_insentif_tl' => $budget_insentif_tl,
                    'img_client' => $img_client,
                    'is_active' => $is_active,
                    'is_delete' => $is_delete,
                    'lock_data' => $lock_data,
                    'lock_data_time' => $lock_data_time,
                    'created_by' => $created_by,
                    'created_date' => $created_date,
                    'update_by' => $update_by,
                    'update_date' => $update_date
                );
                $this->db->where($where);
                $update = $this->db->update('client', $data);

                if ($update) {

                    $response['status']=200;
                    $response['error']=false;
                    $response['message']='Data client berhasil di update.';
                    return $response;
                } else {

                    $response['status']=502;
                    $response['error']=true;
                    $response['message']='Data client gagal di update.';
                    return $response;
                }
            }
        }

        public function delete_client($id_client)
        {
            if ($id_client == "") {
                return $this->empty_response();
            } else {
                $where = array('id_client' => $id_client);

                $this->db->where($where);
                    $delete = $this->db->delete('client');
                    if ($delete) {

                        $response['status']=200;
                        $response['error']=false;
                        $response['message']='Data client dihapus.';
                        return $response;
                    } else {

                        $response['status']=502;
                        $response['error']=true;
                        $response['message']='Data client gagal dihapus.';
                        return $response;
                    }
            }
        }
    }
    
?>