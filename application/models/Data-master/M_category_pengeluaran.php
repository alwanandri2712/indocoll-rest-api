<?php

 class M_category_pengeluaran extends CI_Model
 {
     public function empty_response()
     {
        $response['status'] = 200;
        $response['error'] = true;
        $response['message'] = 'field tidak boleh kosong';
     }

     public function all_category()
     {
         $category = $this->db->get('category_pengeluaran')->result();
         $response['meesage'] = 'berhasil get data category';
            $response['status'] = 200;
            $response['error'] = false;
            $response['category_pengeluaran'] = $category;
            return $response;
     }

     public function add_category($id_category_pengeluaran, $code_akun, $nama_category, $created_by, $created_date)
     {
         if (empty($id_category_pengeluaran) || empty($code_akun) || empty($nama_category) || empty($created_by)) 
         {
             return $this->empty_response();
         } else {

            $data = array (
                
                'id_category_pengeluaran' => $id_category_pengeluaran,
                'code_akun' => $code_akun,
                'nama_category' => $nama_category,
                'created_by' => $created_by,
                'created_date' => $created_date
            );
            $insert = $this->db->insert('category_pengeluaran', $data);

            if ($insert) {

                $response['status']=200;
                $response['error']=false;
                $response['message']='Data category pengeluaran ditambahkan.';
                return $response; 
            } else {

                $response['status']=502;
                $response['error']=true;
                $response['message']='Data category pengeluaran gagal ditambahkan.';
                return $response;
            }
         }
     }

     public function edit_category($id_category_pengeluaran, $code_akun, $nama_category, $created_by, $created_date)
     {
         if (empty($id_category_pengeluaran) || empty($code_akun) || empty($nama_category) || empty($created_by))
         {
             return $this->empty_response();

         } else {

            $where = array(
                'id_category_pengeluaran' => $id_category_pengeluaran
            );
            $data = array (
                
                'id_category_pengeluaran' => $id_category_pengeluaran,
                'code_akun' => $code_akun,
                'nama_category' => $nama_category,
                'created_by' => $created_by,
                'created_date' => $created_date
            );
            $this->db->where($where);
            $update = $this->db->update('category_pengeluaran', $data);
            // var_dump($update); die;

            if ($update) {
                
                $response['status']=200;
                $response['error']=false;
                $response['message']='Data category pengeluaran diubah.';
                return $response;
            } else {

                $response['status']=502;
                $response['error']=false;
                $response['message']='Data category pengeluaran gagal diubah.';
                return $response;
            }
         }
     }

     public function delete_category($id_category_pengeluaran)
        {
            if ($id_category_pengeluaran == "") {
                return $this->empty_response();
            } else {
                $where = array('id_category_pengeluaran' => $id_category_pengeluaran);

                $this->db->where($where);
                    $delete = $this->db->delete('category_pengeluaran');
                    if ($delete) {

                        $response['status']=200;
                        $response['error']=false;
                        $response['message']='Data category pengeluaran dihapus.';
                        return $response;
                    } else {

                        $response['status']=502;
                        $response['error']=true;
                        $response['message']='Data category pengeluaran gagal dihapus.';
                        return $response;
                    }
            }
        }
 }
 

?>