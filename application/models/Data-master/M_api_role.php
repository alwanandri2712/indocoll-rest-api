<?php

class M_api_role extends CI_Model
{
    public function empty_response()
    {
        $response['status'] = 200;
        $response['error'] = true;
        $response['message'] = 'field tidak boleh kosong';
    }

    public function all_role()
    {
        $role = $this->db->get('role')->result();
        $response['meesage'] = 'berhasil get data role';
        $response['status'] = 200;
        $response['error'] = false;
        $response['role'] = $role;
        return $response;
    }

    public function add_role($id_role, $nama_role, $uang_kehadiran, $uang_keterlambatan, $ttl_target, $is_active, $is_delete, $created_by, $created_date, $update_by, $update_date)
    {
        if (empty($id_role) || empty($nama_role) || empty($uang_kehadiran) || empty($uang_keterlambatan) || empty($ttl_target) || empty($created_by) || empty($created_date) || empty($update_by) || empty($update_date)) 
        {
            return $this->empty_response();
        } else {

            $data = array(
                'id_role' => $id_role,
                'nama_role' => $nama_role,
                'uang_kehadiran' => $uang_kehadiran,
                'uang_keterlambatan' => $uang_keterlambatan,
                'ttl_target' => $ttl_target,
                'is_active' => $is_active,
                'is_delete' => $is_delete,
                'created_by' => $created_by,
                'created_date' => $created_date,
                'update_by' => $update_by,
                'update_date' => $update_date
            );
            $insert = $this->db->insert('role', $data);
            // var_dump($insert); die;

            if ($insert) {
                
                $response['status']=200;
                $response['error']=false;
                $response['message']='Data role ditambahkan.';
                return $response;
            } else {

                $response['status']=502;
                $response['error']=true;
                $response['message']='Data role gagal ditambahkan.';
                return $response;
            }
        }
    }

    public function edit_role($id_role, $nama_role, $uang_kehadiran, $uang_keterlambatan, $ttl_target, $is_active, $is_delete, $created_by, $created_date, $update_by, $update_date)
    {
        if (empty($id_role) || empty($nama_role) || empty($uang_kehadiran) || empty($uang_keterlambatan) || empty($ttl_target) || empty($created_by) || empty($created_date) || empty($update_by) || empty($update_date)) {
            return $this->empty_response();
        } else {
            $where = array(
                'id_role' => $id_role
            );

            $data = array(
                'id_role' => $id_role,
                'nama_role' => $nama_role,
                'uang_kehadiran' => $uang_kehadiran,
                'uang_keterlambatan' => $uang_keterlambatan,
                'ttl_target' => $ttl_target,
                'is_active' => $is_active,
                'is_delete' => $is_delete,
                'created_by' => $created_by,
                'created_date' => $created_date,
                'update_by' => $update_by,
                'update_date' => $update_date
            );
            $this->db->where($where);
            $update = $this->db->update('role', $data);

            if ($update) {
                    $response['status']=200;
                    $response['error']=false;
                    $response['message']='Data role diubah.';
                    return $response;
            } else {

                    $response['status']=502;
                    $response['error']=true;
                    $response['message']='Data role gagal diubah.';
                    return $response;
            }
        }
    }

    public function delete_role($id_role)
    {
        if ($id_role == "") {
            return $this->empty_response();
        } else {
            $where = array('id_role' => $id_role);

            $this->db->where($where);
                $delete = $this->db->delete('role');
                if ($delete) {
                    
                    $response['status']=200;
                    $response['error']=false;
                    $response['message']='Data role dihapus.';
                    return $response;
                } else {

                    $response['status']=502;
                    $response['error']=true;
                    $response['message']='Data role gagal dihapus.';
                    return $response;
                }
        }
    }
}

?>