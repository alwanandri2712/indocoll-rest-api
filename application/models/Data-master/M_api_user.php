<?php

class M_api_user extends CI_Model
{
    // response jika ada field yang kosong 
    public function empety_response()
    {
        $response['status'] = 502;
        $response['error'] = true;
        $response['message'] = 'field tidak boleh kosong';
        return $response;
    }

    public function add_user($id_user, $id_role, $id_client, $id_supervisor, $id_team_leader, $nik, $fullname, $username,  $password, $phone, $level, $img_usr, $email_name, $smtp_email, $smtp_password, $is_active, $permission_create, $permission_read, $permission_update, $permission_delete,  $last_login, $lat_long, $user_agents, $ip_addres, $adjust, $code_otp, $created_by,  $created_date,  $update_by, $update_date,  $is_delete)
    {
        if (empty($id_user) || empty($id_role) || empty($id_client) || empty($id_supervisor) || empty($id_team_leader) || empty($nik) || empty($fullname) || empty($username) || empty($password) || empty($phone) || empty($level) || empty($img_usr) || empty($email_name) || empty($smtp_email) || empty($smtp_password) || empty($is_active)) 
        {
            return $this->empety_response();
            
        } else {
            $data = array(
            'id_user'   => $id_user,
            'id_role'   => $id_role,
            'id_client' => $id_client,
            'id_supervisor' => $id_supervisor,
            'id_team_leader' => $id_team_leader,
            'nik' => $nik,
            'fullname' => $fullname,
            'username' => $username,
            'password' => $password,
            'phone' => $phone,
            'level' => $level,
            'img_usr' => $img_usr,
            'email_name' => $email_name,
            'smtp_email' => $smtp_email,
            'smtp_password' => $smtp_password,
            'is_active' => $is_active,
            'permission_create' => $permission_create,
            'permission_read' => $permission_read,
            'permission_update' => $permission_update,
            'permission_delete' => $permission_delete,
            'last_login' => $last_login,
            'lat_long' => $lat_long,
            'user_agents' => $user_agents,
            'ip_addres' => $ip_addres,
            'adjust' => $adjust,
            'code_otp' => $code_otp,
            'created_by' => $created_by,
            'created_date' => $created_date,
            'update_by' => $update_by,
            'update_date' => $update_date,
            'is_delete' => $is_delete
            );

            $insert = $this->db->insert('user', $data);
            // var_dump($insert); die;

            if ($insert) {
                $response['status']=200;
                $response['error']=false;
                $response['message']='Data user ditambahkan.';
                return $response;
            } else {
                $response['status']=502;
                $response['error']=true;
                $response['message']='Data user gagal ditambahkan.';
                return $response;
            }
        }
    }

    // mengambil semua data user
    public function all_user()
    {
        $all = $this->db->get('user')->result();
        $response['meesage'] = 'berhasil get data user';
        $response['status'] = 200;
        $response['error'] = false;
        $response['user'] = $all;
        return $response;
    }

    // hapus data-master di tbl_user
    public function delete_user($id_user)
    {
        if ($id_user == "") {
            return $this->empety_response();
        } else {
            $where = array( 'id_user' => $id_user);

        $this->db->where($where);
            $delete = $this->db->delete('user');
            if ($delete) {
                $response['status']=200;
                $response['error']=false;
                $response['message']='Data person dihapus.';
                return $response;
            } else {
                $response['status']=502;
                $response['error']=true;
                $response['message']='Data person gagal dihapus.';
                return $response;
            }
        }
    }
    // update data-master user
    public function update_user($id_user, $id_role, $id_client, $id_supervisor, $id_team_leader, $nik, $fullname, $username,  $password, $phone, $level, $img_usr, $email_name, $smtp_email, $smtp_password, $is_active, $permission_create, $permission_read, $permission_update, $permission_delete,  $last_login, $lat_long, $user_agents, $ip_addres, $adjust, $code_otp, $created_by,  $created_date,  $update_by, $update_date,  $is_delete)
    {
        if (empty ($id_user) || empty ($id_role) || empty ($id_client) || empty($id_supervisor) || empty($id_team_leader) || empty($nik) || empty($fullname) || empty($username) || empty($password) || empty($phone) || empty($level) || empty($img_usr) || empty($email_name) || empty($smtp_email) || empty($smtp_password) || empty($is_active) || empty($permission_create) || empty($permission_read) || empty($permission_update) || empty($permission_delete) || empty($last_login) || empty($lat_long) || empty($user_agents) || empty($ip_addres) || empty($adjust) || empty($code_otp) || empty($created_by) || empty($created_date) || empty($update_by) || empty($update_date) || empty($is_delete)) {
            return $this->empety_response();
        } else {
            $where = array(
                'id_user' => $id_user
            );

        $set = array(
            'id_role'   => $id_role,
            'id_client' => $id_client,
            'id_supervisor' => $id_supervisor,
            'id_team_leader' => $id_team_leader,
            'nik' => $nik,
            'fullname' => $fullname,
            'username' => $username,
            'password' => $password,
            'phone' => $phone,
            'level' => $level,
            'img_usr' => $img_usr,
            'email_name' => $email_name,
            'smtp_email' => $smtp_email,
            'smtp_password' => $smtp_password,
            'is_active' => $is_active,
            'permission_create' => $permission_create,
            'permission_read' => $permission_read,
            'permission_update' => $permission_update,
            'permission_delete' => $permission_delete,
            'last_login' => $last_login,
            'lat_long' => $lat_long,
            'user_agents' => $user_agents,
            'ip_addres' => $ip_addres,
            'adjust' => $adjust,
            'code_otp' => $code_otp,
            'created_by' => $created_by,
            'created_date' => $created_date,
            'update_by' => $update_by,
            'update_date' => $update_date,
            'is_delete' => $is_delete
        );
        $this->db->where($where);
            $update = $this->db->update('user', $set);
            if ($update) {
                $response['status']=200;
                $response['error']=false;
                $response['message']='Data person diubah.';
                return $response;
            } else {
                $response['status']=502;
                $response['error']=true;
                $response['message']='Data person gagal diubah.';
                return $response;
            }
        }
    }
    
}

?>