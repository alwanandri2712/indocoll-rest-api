<?php

    class M_absen extends CI_Model
    {
        public $tanggal_skarang;

    	public function __construct()
    	{
            $this->tanggal_skarang = date("Y-m-d");;
    		$this->load->library('session');
    	}

        public function query_get_laporan_absen($id_user)
        {
            return $this->db->query("SELECT * FROM laporan_absen WHERE waktu_absen_keluar IS NULL AND tanggal = '$this->tanggal_skarang' AND id_user = '$id_user'");
        }

        public function sql_laporan_absen($id_user)
        {
            return $this->db->query("SELECT * FROM laporan_absen WHERE id_user = '$id_user' AND tanggal = '$this->tanggal_skarang' LIMIT 1 ");
        }

        public function res_config()
        {
            return $this->db->from('config')->get()->row();
        }
    }

?>