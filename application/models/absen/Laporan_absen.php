<?php

    class Laporan_absen extends CI_Model
    {
    	public $table;
    	
    	public function __construct()
    	{
    		$this->table = "laporan_absen";
    		$this->db = $this->load->database('pt_indocoll', true);
    	}

    	public function query($query)
    	{
            return $this->db->query($query);
    	}

        public function absen_report()
        {
            return $this->db->query("SELECT * FROM laporan_absen INNER JOIN user ON laporan_absen.id_user=user.id_user INNER JOIN client ON laporan_absen.id_client=client.id_client");
        }

        /* Query Konfirmasi Absen */
        public function ajaxKonfirmasiAbsen($tanggal_absen)
        {
        	$column_search = array('id_laporan', 'nama_user', 'tanggal');
        	$column_order = array('id_laporan', 'nama_user', 'tanggal');

        	$this->db->from($this->table);
        	$this->db->join('user', 'laporan_absen.id_user = user.id_user');
        	// $this->db->join('client', 'laporan_absen.id_client = client.id_client');
        	$this->db->where('status', 'N');
        	$this->db->where('laporan_absen.tanggal', $tanggal_absen);

        	$i = 0;
	        foreach ($column_search as $item) // loop kolom 
	        {
	            if ($this->input->post('search')['value']) // jika datatable mengirim POST untuk search
	            {
	                if ($i === 0) // looping pertama
	                {
	                    $this->db->group_start();
	                    $this->db->like($item, $this->input->post('search')['value']);
	                } else {
	                    $this->db->or_like($item, $this->input->post('search')['value']);
	                }
	                if (count($column_search) - 1 == $i) //looping terakhir
	                    $this->db->group_end();
	            }
	            $i++;
	        }

        	// jika datatable mengirim POST untuk order
	        if ($this->input->post('order')) {
	            $this->db->order_by($column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
	        } else if (isset($this->order)) {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
        }

        public function get_datatables_konfirmasi_absen($tanggal_absen)
	    {
	        $this->ajaxKonfirmasiAbsen($tanggal_absen);
	        if ($this->input->post('length') != -1)
	            $this->db->limit($this->input->post('length'), $this->input->post('start'));
	        $query = $this->db->get();
	        // echo $this->db->last_query();
	        return $query->result();
	    }

	    public function count_filtered_konfirmasi_absen($tanggal_absen)
	    {
	        $this->ajaxKonfirmasiAbsen($tanggal_absen);
	        $query = $this->db->get();
	        return $query->num_rows();
	    }

	    public function count_all_konfirmasi_absen($tanggal_absen)
	    {
	        $this->db->from($this->table);
	        $this->db->where('status', 'N');
	        $this->db->where('tanggal', $tanggal_absen);
	        return $this->db->count_all_results();
	    }

	    /* Laporan Absen Query */
        public function ajaxLaporanAbsen($start_date, $end_date)
        {
        	$column_search = array('id_laporan', 'nama_user', 'tanggal');
        	$column_order = array('id_laporan', 'nama_user', 'tanggal');

        	$this->db->from($this->table);
        	$this->db->join('user', 'laporan_absen.id_user = user.id_user');

        	if($start_date != "" && $end_date != ""){
        		$this->db->where("tanggal BETWEEN '$start_date' AND '$end_date'");
        	}
        	
        	$i = 0;
	        foreach ($column_search as $item) // loop kolom 
	        {
	            if ($this->input->post('search')['value']) // jika datatable mengirim POST untuk search
	            {
	                if ($i === 0) // looping pertama
	                {
	                    $this->db->group_start();
	                    $this->db->like($item, $this->input->post('search')['value']);
	                } else {
	                    $this->db->or_like($item, $this->input->post('search')['value']);
	                }
	                if (count($column_search) - 1 == $i) //looping terakhir
	                    $this->db->group_end();
	            }
	            $i++;
	        }

        	// jika datatable mengirim POST untuk order
	        if ($this->input->post('order')) {
	            $this->db->order_by($column_order[$this->input->post('order')['0']['column']], $this->input->post('order')['0']['dir']);
	        } else if (isset($this->order)) {
	            $order = $this->order;
	            $this->db->order_by(key($order), $order[key($order)]);
	        }
        }

        public function get_datatables_laporan_absen($start_date, $end_date)
	    {
	        $this->ajaxLaporanAbsen($start_date, $end_date);
	        if ($this->input->post('length') != -1)
	            $this->db->limit($this->input->post('length'), $this->input->post('start'));
	        $query = $this->db->get();
	        // echo $this->db->last_query();
	        return $query->result();
	    }

	    public function count_filtered_laporan_absen($start_date, $end_date)
	    {
	        $this->ajaxLaporanAbsen($start_date, $end_date);
	        $query = $this->db->get();
	        return $query->num_rows();
	    }

	    public function count_all_laporan_absen()
	    {
	        $this->db->from($this->table);
	        return $this->db->count_all_results();
	    }
		
		public function res_config()
        {
            return $this->db->query("SELECT * FROM config")->row();
        }
    }

?>
