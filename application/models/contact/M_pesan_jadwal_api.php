<?php

class M_pesan_jadwal_api extends CI_Model
{
    public function empty_response()
    {
        $response['status'] = 200;
        $response['error'] = true;
        $response['message'] = 'field tidak boleh kosong';
        return $response;
    }

    public function add_pesan_jadwal($id_pesan_jadwal, $fk_id_user, $sender_wa, $nama, $no_telp, $pesan, $jadwal, $status, $created_date, $created_by, $is_delete)
    {
        if (empty($id_pesan_jadwal) || empty($fk_id_user) || empty($nama) || empty($no_telp) || empty($pesan) || empty($jadwal) || empty($status) || empty($created_date) || empty($created_by)) 
        {
            return $this->empty_response();
        } else {
            $data = array(
                'id_pesan_jadwal' => $id_pesan_jadwal,
                'fk_id_user' => $fk_id_user,
                'sender_wa' => $sender_wa,
                'nama' => $nama,
                'no_telp' => $no_telp,
                'pesan' => $pesan,
                'jadwal' => $jadwal,
                'status' => $status,
                'created_date' => $created_date,
                'created_by' => $created_by,
                'is_delete' => $is_delete
            );
            $insert = $this->db->insert('pesan_jadwal', $data);

            if ($insert) {

                $response['status']=200;
                $response['error']=false;
                $response['message']='Data user ditambahkan.';
                return $response;

            } else {

                $response['status']=502;
                $response['error']=true;
                $response['message']='Data user gagal ditambahkan.';
                return $response;
            }
        }
    }

    // gett all data jadwal pemesanan
    public function all_user()
    {
        $all = $this->db->get('pesan_jadwal')->result();
        $response['meesage'] = 'berhasil get data pesan_jadwal';
        $response['status'] = 200;
        $response['error'] = false;
        $response['pesan_jadwal'] = $all;
        return $response;
    }

    // update data pesan_jadwal 
    public function update_pesan_jadwal($id_pesan_jadwal, $fk_id_user, $sender_wa, $nama, $no_telp, $pesan, $jadwal, $status, $created_date, $created_by, $is_delete)
    {
        if (empty($id_pesan_jadwal) || empty($fk_id_user) || empty($nama) || empty($no_telp) || empty($pesan) || empty($jadwal) || empty($status) || empty($created_date) || empty($created_by)) {
            return $this->empty_response();
        } else {
            $where = array(
                'id_pesan_jadwal' => $id_pesan_jadwal
            );
            $set = array(
                'id_pesan_jadwal' => $id_pesan_jadwal,
                'fk_id_user' => $fk_id_user,
                'sender_wa' => $sender_wa,
                'nama' => $nama,
                'no_telp' => $no_telp,
                'pesan' => $pesan,
                'jadwal' => $jadwal,
                'status' => $status,
                'created_date' => $created_date,
                'created_by' => $created_by,
                'is_delete' => $is_delete
            );
            $this->db->where($where);
                $update = $this->db->update('pesan_jadwal', $set);
                // var_dump($update); die;
                if ($update) {
                    $response['status']=200;
                    $response['error']=false;
                    $response['message']='Data person diubah.';
                    return $response;
                } else {
                    $response['status']=502;
                    $response['error']=true;
                    $response['message']='Data person gagal diubah.';
                    return $response;
                }
        }
    }
    // delete pesan_jadwal
    public function delete_pesan_jadwal($id_pesan_jadwal)
    {
        if ($id_pesan_jadwal == "") {
            return $this->empty_response();
        } else {
            $where = array('id_pesan_jadwal' => $id_pesan_jadwal);
            
            $this->db->where($where);
                $delete = $this->db->delete('pesan_jadwal');
                if ($delete) {
                    $response['status']=200;
                    $response['error']=false;
                    $response['message']='Data pesan_jadwal dihapus.';
                    return $response;
                } else {
                    $response['status']=502;
                    $response['error']=true;
                    $response['message']='Data pesan_jadwal gagal dihapus.';
                    return $response;
            }
        }
    }
}

?>