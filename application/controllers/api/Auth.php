<?php
require APPPATH . '/libraries/REST_Controller.php';

use PhpParser\Node\Stmt\TryCatch;
use Restserver\Libraries\REST_Controller;

class Auth extends REST_Controller
 {
     public function __construct()
     {
         parent::__construct();
         $this->load->database();
         $this->load->model('M_auth');
     }

     public function checkLogin_post()
     {
        $nik      = $this->post('nik');
        $password = md5($this->post('password'));
        $data = array(
            'nik' => $nik,
            'password' => $password
        );
        $check     = $this->M_auth->check_auth('user', $data)->num_rows();
        $data_user = $this->M_auth->check_auth('user', $data)->result();
        
        if ($check > 0) {
            $response = ['status' => 'success', 'msg' => 'Login Berhasil'];
            return $this->response($response, REST_Controller::HTTP_OK);
        } else {
            $response = ['status' => 'failed', 'msg' => 'Gagal Login'];
            return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
        }
     }
 }
 

?>