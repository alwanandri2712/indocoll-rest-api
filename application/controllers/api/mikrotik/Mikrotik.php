<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require APPPATH . '/libraries/router.class.php';

// use Libraries\RouterosAPI;
use Restserver\Libraries\REST_Controller;

class Mikrotik extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        
        $this->router = new RouterosAPI();
        $this->router->debug = false;
        $this->load->database();   
    }

    public function check_connection_get()
    {
    	if ($this->router->connect()) {
    		$response = array('status' => 'success', 'msg' => 'Connected Mikrotik');
    		return $this->response($response, REST_Controller::HTTP_OK);
    	}else{
    		$response = array('status' => 'failed', 'msg' => 'Disconect Mikrotik');
    		return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
    	}
    }

    public function listcpu_get()
    {
    	$getresource = $this->router->comm("/system/resource/print");
      	$getrouterboard = $this->router->comm("/system/routerboard/print");
      	$resource = $getresource; // resources
      	$routerboard = $getrouterboard; // routerboard
      	$response_load = array();
      	$response_board = array();
      	foreach($resource as $key)
     	{
         	array_push($response_load, [
	            'cpu_load' => $key['cpu-load'] ?? "CPU CONNECT FAILS",
	            'free_memory' => $Lib->formatBytes($key['free-memory'], 2),
	            'free_hdd' => $Lib->formatBytes($key['free-hdd-space'], 2),
	            'router_os' => $key['version'],
	            'board_name' => $key['board-name'],
	            'uptime' => $API->formatDTM($key['uptime'])
         	]);
      	}
      	foreach($routerboard as $key)
      	{
         	array_push($response_board, array(
            	'model' => $key['model'],
            	'serial-number' => $key['serial-number'],
         	));
      	}
      	$response =array(
      		'status' 	=> 'success',
      		'msg' 		=> 'Connected',
         	'load' 		=> $response_load,
         	'board' 	=> $response_board
      	);
      	
      	$this->response($response, REST_Controller::HTTP_OK);
    }

}