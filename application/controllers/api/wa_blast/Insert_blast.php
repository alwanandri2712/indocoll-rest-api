<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . 'third_party/vendor/autoload.php';
use Enqueue\Redis\RedisConnectionFactory;

class Insert_blast extends CI_Controller
{

    public function __construct()
    {
        $this->connectionFactory = new RedisConnectionFactory(
            [
                'scheme'            => 'tcp',
                'host'              => REDIS_HOST,
                'port'              => REDIS_PORT,
                'password'          => REDIS_PASS,
                'scheme_extensions' => [
                    'predis'
                ],
            ]
        );
    }

    public function action()
    {
        $id_user    = $this->input->post('id_user');
        $list_kontak   = $this->input->post('list_kontak');
        $tanggal_kirim = $this->input->post('tanggal_kirim');
        $waktu_kirim   = $this->input->post('waktu_kirim');
        $sender_wa     = $this->input->post('sender_wa');
        $nama          = $this->input->post('nama');
        $pesan         = $this->input->post('message');
        $created_at    = date('Y-m-d H:i:s');
        $created_by    = $this->input->post('created_by');

        $list_kontak  = $this->db->query("SELECT * FROM list_contact WHERE fk_id_users = '$id_user' AND nm_list_cntact ='$list_kontak'")->result();
        $no           = 1;
        $data         = [];
        $min          = 1;
        $max          = 30;
        $random_nomer = rand($min, $max);
        $time         = strtotime($waktu_kirim);
        $cok          = file_get_contents("../App/wa-blast/wa_sessions/whatsapp-sessions.json");
        $fk_id_users  = $id_user;
        $parse_cok    = json_decode($cok);
        $tampung_cok  = array();
         
    }

}
        
    /* End of file  insert_blast.php */
