<?php

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Api_laporan_absen extends REST_Controller
{

    public $table;

    public function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->model('absen/Laporan_absen','LaporanAbsen');
    }

    // Menggunakan Pagination
    public function laporan_absen_post()
    {
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');

        $result = $this->LaporanAbsen->get_datatables_laporan_absen($start_date, $end_date);
        $data = array_filter($result, function ($v) {
            $no = $this->input->post('start');
            $v->no = $no++;
            return $v;
        });

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->LaporanAbsen->count_all_laporan_absen($start_date, $end_date),
            "recordsFiltered" => $this->LaporanAbsen->count_filtered_laporan_absen($start_date, $end_date),
            "data" => $data,
        );
        //output to json format
        $this->response($output, 201);
    }

    // Tanpa Pagination Limit 
    public function laporan_absen_mudah_post()
    {
        $start_date = $this->input->get('start_date');
        $end_date = $this->input->get('end_date');

        $this->db->from($this->table);
        $this->db->join('user', 'laporan_absen.id_user = user.id_user');
        if ($start_date != "" && $end_date != "") {
            $this->db->where("tanggal BETWEEN '$start_date' AND '$end_date'");
        }
        $query = $this->db->get()->result();

        $data = array_filter($query, function ($v) {
            $no = $this->input->post('start');
            $v->no = $no++;
            return $v;
        });

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->LaporanAbsen->count_all_laporan_absen($start_date, $end_date),
            "recordsFiltered" => $this->LaporanAbsen->count_filtered_laporan_absen($start_date, $end_date),
            "data" => $data,
        );
        //output to json format
        $this->response($output, 201);
    }
}
