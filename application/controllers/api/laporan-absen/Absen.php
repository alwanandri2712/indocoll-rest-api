<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Absen extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('absen/M_absen','absen');       
    }

    public function absen_post()
    {
    	if ($this->input->post('submit_absen')) {
    		/* Transcation Database */
    		$this->db->trans_begin();
			
			$id_user       	  = $this->input->post('id_user');
            $tanggal_sekarang = date("Y-m-d");
            $waktu_skrng 	  = date("H:i:s");
            $id_client     	  = $this->input->post('id_client');
            $nama_username 	  = $this->input->post('username');
            $lat_long      	  = $this->input->post('lat_long');
            $image         	  = $this->input->post('image');
            $masuk         	  = $this->input->post('masuk');
            $telat         	  = $this->input->post('telat');

            $folderPath     = "assets/img/users_absen/";
            $image_parts    = explode(";base64,", $image);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type     = $image_type_aux[1];
            $image_base64   = base64_decode($image_parts[1]);
            $fileName       = uniqid() . '.png';
            $file           = $folderPath . $fileName;

            if ($this->session->userdata("level") == 'admin' || $this->session->userdata("level") == 'hrd') {
                $waktu_absen = '07:30:00';
            } else {
                $waktu_absen = date("H:i:s");
            }

             $data = array(
                'id_laporan'    => 'IDCL-'. random_string_idcl(12),
                'id_user'       => $id_user,
                'id_client'     => $id_client,
                'nama_user'     => $nama_username,
                'lat_long'      => $lat_long,
                'capture_image' => $fileName,
                'masuk'         => $masuk,
                'telat'         => $telat,
                'status'        => 'N',
                'tanggal'       => date("Y-m-d"),
                'waktu_absen'   => $waktu_absen,
                'is_delete'     => 0
            );
            
            $data_keluar = array(
            	'nama_user'				=> $nama_username,
                'lat_long'      		=> $lat_long,
                'waktu_absen_keluar' 	=> date("H:i:s"),
                'is_delete'     		=> 0
            );
            if (empty($image)) {
            	$this->db->trans_rollback(); // Transcation Rollback
                $response = ['status' => 'success', 'msg' => 'No Capture'];
        		return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
            } else {
            	$query_get_laporan_absen = $this->db->query("SELECT id_laporan,waktu_absen FROM laporan_absen WHERE tanggal = '$tanggal_sekarang' AND id_user = '$id_user' ")->result();
                if (empty($query_get_laporan_absen)) {

                	$result = $this->db->insert('laporan_absen', $data);
                	
                	if ($this->db->trans_status() === TRUE) {

                		file_put_contents($file, $image_base64); // insert photos    	
                		
                		$resconfig = $this->absen->res_config();
	                    if (substr($waktu_skrng,-8) > $resconfig->waktu_absen) {
	                        $status = "Terlambat";
	                    } else {
	                        $status = "Tepat Waktu";
	                    }
	                    $this->db->trans_commit();  // Transcation Success
                    	$response = ['status' => 'success', 'msg' => 'Berhasil Absen Masuk', 'data' => $data];
                		return $this->response($response, REST_Controller::HTTP_OK);

                	}else{
                		$this->db->trans_rollback(); // Transcation Rollback
                    	$response = ['status' => 'error', 'msg' => 'Gagal Absen Masuk'];
                		return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
                	}
                	
                }else{
                	// file_put_contents($file, $image_base64);
                    $where_laporan = [ 'id_laporan' => $query_get_laporan_absen[0]->id_laporan ];
                    $result = $this->db->update("laporan_absen", $data_keluar, $where_laporan);
                    if ($this->db->trans_status() === TRUE) {
                    	$this->db->trans_commit(); // Transcation Success
                        $response = ['status' => 'success', 'msg' => 'Berhasil Absen Keluar', 'data' => $data_keluar];
                		return $this->response($response, REST_Controller::HTTP_OK);
                    } else {
                    	$this->db->trans_rollback(); // Transcation Rollback
                		$response = ['status' => 'error', 'msg' => 'Gagal Absen Keluar'];
                		return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
                    }
                }
            }
		}else{
			$this->db->trans_rollback(); // Transcation Rollback
			$response = ['status' => 'error', 'msg' => 'Submit Absen Gagal'];
    		return $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
		}
    }
}