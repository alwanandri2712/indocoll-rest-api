<?php
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class KonfirmasiAbsen extends REST_Controller

{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('absen/Konfirmasiabsen_model','konfirmasiabsen');
    }

    public function ajax_datatable_post(){
        $data_konfirmasiabsen = $this->konfirmasiabsen->get_datatables();
        $data = array_filter($data_konfirmasiabsen, function($v) {
            $v->image = 'https://appindocoll/assets/img/users_absen/'.$v->capture_image;
            unset($v->last_update);
            unset($v->ip_user);
            unset($v->ua_user);
            return $v;
        });

        $response = array(
            "draw"            => $this->input->post('draw'),
            "recordsTotal"    => $this->konfirmasiabsen->count_all(),
            "recordsFiltered" => $this->konfirmasiabsen->count_filtered(),
            "data"            => $data,
        );

        $this->response($response);
    }

    public function add_post(){
        $data = array(
            'id_laporan'  => $this->post('id_laporan'),
            'id_user'     => $this->post('id_user'),
            'status'      => $this->post('status'),
            'last_update' => $this->post('last_update'),
            'ip_user'     => $this->post('ip_user'),
            'ua_user'     => $this->post('ua_user'),
        );
        try {
            $insert = $this->konfirmasiabsen->insert($data);
        } catch (\Throwable $th) {
            //throw $th;
        }

        $response = array(
            'status' => true,
            'message' => 'Data berhasil ditambahkan',
        ); 
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function update_update(){
        $id = $this->post('id_laporan');
        $data = array(
            'nama_user'   => $this->post('nama_user'),
            'status'      => $this->post('status'),
            'tanggal'     => $this->post('tanggal'),
            'waktu_absen' => $this->post('waktu_absen'),
        );

        try {
            $this->konfirmasiabsen->update($id,$data);
        } catch (\Throwable $th) {
            //throw $th;
        }

        $response = array(
            "status" => "success",
            "message" => "Data berhasil diupdate",
            "data" => $data
        );
        $this->response($response);

    }

    public function delete_delete(){
        $id = $this->delete('id_laporan_absen');
        try {
            $this->konfirmasiabsen->delete($id);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function updateStatus_post(){
        $id = $this->post('id_laporan_absen');
        $data = array(
            'status' => "Y"
        );
        try {
            $data = $this->konfirmasiabsen->updateStatus($id, $data);
        } catch (\Throwable $th) {
            $data = $th->getMessage();
                
        }

        $response = [
            'status' => true,
            'data' => $data
        ];
        $this->response($response, REST_Controller::HTTP_OK);
    }

}