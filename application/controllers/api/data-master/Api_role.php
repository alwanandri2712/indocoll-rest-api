<?php

 require APPPATH . '/libraries/REST_Controller.php';
 use Restserver\Libraries\REST_Controller;

 class Api_role extends REST_Controller
 {
     public function __construct()
     {
         parent:: __construct();
         $this->load->database();  
         $this->load->model('Data-master/M_api_role');  
     }

     public function index_get()
     {
         $response = $this->M_api_role->all_role();
         $this->response($response);
     }

     public function index_post()
     {
         $response = $this->M_api_role->add_role(
             $this->post('id_role'),
             $this->post('nama_role'),
             $this->post('uang_kehadiran'),
             $this->post('uang_keterlambatan'),
             $this->post('ttl_target'),
             $this->post('is_active'),
             $this->post('is_delete'),
             $this->post('created_by'),
             $this->post('created_date'),
             $this->post('update_by'),
             $this->post('update_date'),
         );
         $this->response($response);
     }

     public function index_put()
     {
         $response = $this->M_api_role->edit_role(
             $this->put('id_role'),
             $this->put('nama_role'),
             $this->put('uang_kehadiran'),
             $this->put('uang_keterlambatan'),
             $this->put('ttl_target'),
             $this->put('is_active'),
             $this->put('is_delete'),
             $this->put('created_by'),
             $this->put('created_date'),
             $this->put('update_by'),
             $this->put('update_date'),
         );
         $this->response($response);
     }

     public function index_delete()
     {
         $response = $this->M_api_role->delete_role(
             $this->delete('id_role')
         );
         $this->response($response);
     }
     
 }

?>
