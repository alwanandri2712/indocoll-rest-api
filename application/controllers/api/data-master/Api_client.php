  <?php

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Api_client extends REST_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->database();
        $this->load->model('Data-master/M_api_client');
    }

    public function client_get()
    {
        $response = $this->M_api_client->all_client();
        $this->response($response);
        
    }

    public function client_post()
    {
        $response = $this->M_api_client->add_client(
            $this->post('id_client'),
            $this->post('nama_aplikasi'),
            $this->post('nama_client'),
            $this->post('ttl_target'),
            $this->post('grade'),
            $this->post('dpd'),
            $this->post('fee'),
            $this->post('budget_insentif'),
            $this->post('bugdet_insentif_tl'),
            $this->post('img_client'),
            $this->post('is_active'),
            $this->post('is_delete'),
            $this->post('lock_data'),
            $this->post('lock_data_time'),
            $this->post('created_by'),
            $this->post('created_date'),
            $this->post('update_by'),
            $this->post('update_date')
        );
        $this->response($response);
    }

    public function client_put()
    {
        $response = $this->M_api_client->update_client(
            $this->put('id_client'),
            $this->put('nama_aplikasi'),
            $this->put('nama_client'),
            $this->put('ttl_target'),
            $this->put('grade'),
            $this->put('dpd'),
            $this->put('fee'),
            $this->put('budget_insentif'),
            $this->put('bugdet_insentif_tl'),
            $this->put('img_client'),
            $this->put('is_active'),
            $this->put('is_delete'),
            $this->put('lock_data'),
            $this->put('lock_data_time'),
            $this->put('created_by'),
            $this->put('created_date'),
            $this->put('update_by'),
            $this->put('update_date')
        );
        $this->response($response);
    }

    public function client_delete()
    {
        $response = $this->M_api_client->delete_client(
            $this->delete('id_client')
        );
        $this->response($response);
    }
}

?>