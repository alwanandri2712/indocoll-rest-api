<?php

 require APPPATH . '/libraries/REST_Controller.php';
 use Restserver\Libraries\REST_Controller;

 class API_user extends REST_Controller
 {
     public function __construct($config = "rest")
     {
         parent::__construct();
         $this->load->database();
         $this->load->model('Data-master/M_api_user');
     }

    // method index untuk menampilkan semua data person menggunakan method get
    public function index_get()
    {
        $response = $this->M_api_user->all_user();
        $this->response($response);
    }

    // menambah user menggunakan method post
    public function add_post()
    {
        $response = $this->M_api_user->add_user(
            // $this->load->library('Uuid'),
            $this->post('id_user'),
            $this->post('id_role'),
            $this->post('id_client'),
            $this->post('id_supervisor'),
            $this->post('id_team_leader'),
            $this->post('nik'),
            $this->post('fullname'),
            $this->post('username'),
            $this->post('password'),
            $this->post('phone'),
            $this->post('level'),
            $this->post('img_usr'),
            $this->post('email_name'),
            $this->post('smtp_email'),
            $this->post('smtp_password'),
            $this->post('is_active'),
            $this->post('permission_create'),
            $this->post('permission_read'),
            $this->post('permission_update'),
            $this->post('permission_delete'),
            $this->post('last_login'),
            $this->post('lat_long'),
            $this->post('user_agents'),
            $this->post('ip_addres'),
            $this->post('adjust'),
            $this->post('code_otp'),
            $this->post('created_by'),
            $this->post('created_date'),
            $this->post('update_by'),
            $this->post('update_date'),
            $this->post('is_delete'),
        );
        $this->response($response);
    }
    
    // update data-master dengan method update 
    public function update_put()
    {
        $response = $this->M_api_user->update_user(
            $this->put('id_user'),
            $this->put('id_role'),
            $this->put('id_client'),
            $this->put('id_supervisor'),
            $this->put('id_team_leader'),
            $this->put('nik'),
            $this->put('fullname'),
            $this->put('username'),
            $this->put('password'),
            $this->put('phone'),
            $this->put('level'),
            $this->put('img_usr'),
            $this->put('email_name'),
            $this->put('smtp_email'),
            $this->put('smtp_password'),
            $this->put('is_active'),
            $this->put('permission_create'),
            $this->put('permission_read'),
            $this->put('permission_update'),
            $this->put('permission_delete'),
            $this->put('last_login'),
            $this->put('lat_long'),
            $this->put('user_agents'),
            $this->put('ip_addres'),
            $this->put('adjust'),
            $this->put('code_otp'),
            $this->put('created_by'),
            $this->put('created_date'),
            $this->put('update_by'),
            $this->put('update_date'),
            $this->put('is_delete'),
        );
        $this->response($response);
    }
    //  hapus data-master user dengan method delete 
    public function delete_delete()
    {
        $response = $this->M_api_user->delete_user(
            $this->delete('id_user')
        );
        $this->response($response);
    }
 }

?>