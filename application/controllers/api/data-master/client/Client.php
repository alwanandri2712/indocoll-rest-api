<?php

defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Client extends REST_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Data-master/client/M_client_model','client_model');
        
    }

    public function index_get(){
        $param['select'] = "nama_aplikasi,nama_client,ttl_target";
        $data = $this->client_model->get_all(null,10,'id_client','desc',$param);
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function ajax_datatable_post(){
        $row = $this->client_model->get_datatables();
        $data = array_filter($row, function($v) {
            return $v;
        });

        $response = array(
            "draw"            => $this->input->post('draw'),
            "recordsTotal"    => $this->client_model->count_all(),
            "recordsFiltered" => $this->client_model->count_filtered(),
            "data"            => $data,
        );

        $this->response($response);
    }

    // public function ajax_datatable_post(){
    //     $list = $this->client_model->get_datatables();
    //     $data = array();

    //     foreach ($list as $row) {
    //         $item     = array();
    //         $item[] = $row->id_client;
    //         $data[] = $item;
    //     }

    //     $response = array(
    //         "draw"            => $this->input->post('draw'),
    //         "recordsTotal"    => $this->client_model->count_all(),
    //         "recordsFiltered" => $this->client_model->count_filtered(),
    //         "data"            => $data,
    //     );

    //     $this->response($response);
    // }

    public function add_post(){
        $data = array(
            'id_laporan'  => $this->uuid->v4(),
            'id_user'     => $this->post('id_user'),
            'status'      => $this->post('status'),
            'last_update' => $this->post('last_update'),
            'ip_user'     => $this->post('ip_user'),
            'ua_user'     => $this->post('ua_user'),
            'created_date' => date('Y-m-d H:i:s'),
        );
        try {
            $insert = $this->client_model->insert($data);
        } catch (\Throwable $th) {
            //throw $th;
        }

        $response = array(
            'status' => true,
            'message' => 'Data berhasil ditambahkan',
        ); 
        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function update_update(){
        $id = $this->post('id_laporan');
        $data = array(
            'nama_user'   => $this->post('nama_user'),
            'status'      => $this->post('status'),
            'tanggal'     => $this->post('tanggal'),
            'waktu_absen' => $this->post('waktu_absen'),
        );

        try {
            $this->client_model->update($id,$data);
        } catch (\Throwable $th) {
            //throw $th;
        }

        $response = array(
            "status" => "success",
            "message" => "Data berhasil diupdate",
            "data" => $data
        );
        $this->response($response);

    }

    public function delete_delete(){
        $id = $this->delete('id_client');
        try {
            $this->client_model->delete($id);
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    public function updateStatus_post(){
        $id = $this->post('id_client');
        $data = array(
            'is_active' => "Y"
        );
        try {
            $data = $this->client_model->updateStatus($id, $data);
        } catch (\Throwable $th) {
            $data = $th->getMessage();
                
        }

        $response = [
            'status' => true,
            'data' => $data
        ];
        $this->response($response, REST_Controller::HTTP_OK);
    }
}
        
    /* End of file  Client.php */
