<?php

    require APPPATH . '/libraries/REST_Controller.php';
    use Restserver\Libraries\REST_Controller;

    class Api_category_pengeluaran extends REST_Controller
    {
        public function __construct()
        {
            parent::__construct();
            $this->load->database();
            $this->load->model('Data-master/M_category_pengeluaran');
        }

        public function category_pengeluaran_get()
        {
            $response = $this->M_category_pengeluaran->all_category();
            $this->response($response);
        }

        public function category_pengeluaran_post()
        {
            $response = $this->M_category_pengeluaran->add_category(
                $this->post('id_category_pengeluaran'),
                $this->post('code_akun'),
                $this->post('nama_category'),
                $this->post('created_by'),
                // $this->post(date('created_date'))
                $this->post('created_date')
            );
            $this->response($response);
        }

        public function category_pengeluaran_put()
        {
            $response = $this->M_category_pengeluaran->edit_category(
                $this->put('id_category_pengeluaran'),
                $this->put('code_akun'),
                $this->put('nama_category'),
                $this->put('created_by'),
                $this->put('created_date')
            );
            $this->response($response);
        }

    public function index_delete()
     {
         $response = $this->M_category_pengeluaran->delete_category(
             $this->delete('id_category_pengeluaran')
         );
         $this->response($response);
     }
    }

?>