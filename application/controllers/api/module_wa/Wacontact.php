<?php

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;
// use Enqueue\Redis\RedisConnectionFactory;

class wacontact extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_wacontact/M_wacontact');
        $this->load->library("pagination");
        $this->load->library('form_validation');

        // Message Broker Redis
        // $this->connectionFactory = new RedisConnectionFactory(
        //     [
        //         'scheme'            => 'tcp',
        //         'host'              => REDIS_HOST,
        //         'port'              => REDIS_PORT,
        //         'password'          => REDIS_PASS,
        //         'scheme_extensions' => [
        //             'predis'
        //         ],
        //     ]
        // );
    }

    public function wa_get()
    {
        $config = array();
        $config["total_rows"] = $this->M_wacontact->get_count();
        $limit = $this->input->get('limit') ?? 0; // set limit
        $config["uri_segment"] = 2; 
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data['limit'] = $limit;
        $data["links"] = $this->pagination->create_links();
        $data['data']  = $this->M_wacontact->get_wacontact($limit, $page);

        return $this->response($data);
        // $response = $this->M_wacontact->get_wacontact(); // tanpa pagination
    }

    public function wa_post()
    {
        $post = $_POST;
        $tampung_post = array();
        
        $this->form_validation->set_rules('phone', 'Phone', 'required|min_length[3]', array('required' => 'Columns %s is required', 'min_length' => 'Columns %s kurang dari 3'));
        $this->form_validation->set_rules('nama_nasabah', 'Nama Nasabah', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            return $this->response(['data' => validation_errors()]);
            exit;
        }

        foreach ($post as $key => $value) {
            $tampung_post += [$key => $_POST[$key]];
        }
        $result = $this->M_wacontact->post_wacontact($tampung_post);
        if ($result == 1) {
            return $this->response(['status' => 200, 'data' => $tampung_post], 201);
        }else{
            return $this->response(['status' => 400, 'data' => array()], 401);
        }
        // $response = $this->M_wacontact->post_wacontact(
        //     $this->post('id_history_blast'),
        //     $this->post('fk_id_user'),
        //     $this->post('fk_id_wa_client'),
        //     $this->post('nm_broadcast'),
        //     $this->post('nama_nasabah'),
        //     $this->post('phone'),
        //     $this->post('somasi'),
        //     $this->post('status'),
        //     $this->post('ttl_tagihan')
        // );
        // $this->response($response);
    }
}
