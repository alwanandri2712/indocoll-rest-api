<?php

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class skiptrace extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('M_skiptrace/M_skiptrace', 'skiptrace');
    }

    public function index_get()
    {
        // $param['select'] = "nama_barang,status,keterangan";
        $param['where'] = array(
            'nama_barang' => "Lenovo-T420",
            'status'      => "Y",
        );
        $data = $this->inventory_barang->get_all(null, null, 'id_barang', 'DESC', $param);
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function ajax_datatable_post()
    {
        $row = $this->inventory_barang->get_datatables();
        $data = array_filter($row, function ($v) {
            return $v;
        });

        $response = array(
            "draw"            => $this->input->post('draw'),
            "recordsTotal"    => $this->client_model->count_all(),
            "recordsFiltered" => $this->client_model->count_filtered(),
            "data"            => $data,
        );

        $this->response($response);
    }

    public function addTL_post()
    {

        // input post  data 
        $id_skip_trash    = $this->post('id_skip_trash');
        $fk_id_users      = $this->post('fk_id_users');
        $fk_id_client     = $this->post('fk_id_client');
        $no_phone_nasabah = $this->post('no_phone_nasabah');
        $alamat_nasabah   = $this->post('alamat_nasabah');
        $email_nasabah    = $this->post('email_nasabah');
        $nik_nasabah      = $this->post('nik_nasabah');
        $nama_nasabah     = $this->post('nama_nasabah');
        $created_date     = date('Y-m-d H:i:s');
        $uuid_nasabah     = $this->post('uuid_nasabah');

        $data = array(
            'id_skip_trash'    => $id_skip_trash,
            'fk_id_users'      => $fk_id_users,
            'fk_id_client'     => $fk_id_client,
            'no_phone_nasabah' => $no_phone_nasabah,
            'alamat_nasabah'   => $alamat_nasabah,
            'email_nasabah'    => $email_nasabah,
            'nik_nasabah'      => $nik_nasabah,
            'nama_nasabah'     => $nama_nasabah,
            'created_date'     => $created_date,
            'uuid_nasabah'     => $uuid_nasabah

        );
        $insert = $this->skip_trash->insert($data);
        if (!$insert) {
            $response['status']  = 500;
            $response['error']   = true;
            $response['message'] = 'data gagal di input';
            $this->response($response, 400);
        } else {
            $this->response('data berhasil di input', 200);
        }
    }

    public function update_post()
    {
        // input post  data 
        $instagram       = $this->post('instagram');
        $facebook        = $this->post('facebook');
        $lainnya         = $this->post('lainnya');

        $data = array(
            'instagram'       => $instagram,
            'facebook'        => $facebook,
            'lainnya'         => $lainnya,
        );

        $update = $this->skip_trash->update($instagram, $facebook, $lainnya);

        if (!$update) {

            $response['status']  = 500;
            $response['error']   = true;
            $response['message'] = 'data gagal di input';
            $this->response($response, 400);
        } else {
            $this->response('data berhasil di input', 200);
        }
    }

    public function delete_post()
    {
        $fk_id_users = $this->post('fk_id_users');
        $delete    = $this->model_inventory->delete($fk_id_users);
        if ($delete) {
            $response['status']  = 200;
            $response['error']   = false;
            $response['message'] = 'Data user dihapus.';
            return $this->response($response);
        } else {
            $response['status']  = 400;
            $response['error']   = true;
            $response['message'] = 'Data user gagal dihapus.';
            return $this->response($response);
        }
    }
}
