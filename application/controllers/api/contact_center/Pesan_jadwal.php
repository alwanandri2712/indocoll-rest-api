<?php

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Pesan_jadwal extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('contact/M_pesan_jadwal_api');
    }

    public function index_get()
    {
        $response = $this->M_pesan_jadwal_api->all_user();
        $this->response($response);
    }

    // post data
     public function add_post()
    {
        $response = $this->M_pesan_jadwal_api->add_pesan_jadwal(
            // $this->load->library('Uuid'),
            $this->post('id_pesan_jadwal'),
            $this->post('fk_id_user'),
            $this->post('sender_wa'),
            $this->post('nama'),
            $this->post('no_telp'),
            $this->post('pesan'),
            $this->post('jadwal'),
            $this->post('status'),
            $this->post('created_date'),
            $this->post('created_by'),
            $this->post('is_delete'),
           
        );
        $this->response($response);
    }
    
    // update data 
    public function update_put()
    {
        
        $response = $this->M_pesan_jadwal_api->update_pesan_jadwal(
            $this->put('id_pesan_jadwal'),
            $this->put('fk_id_user'),
            $this->put('sender_wa'),
            $this->put('nama'),
            $this->put('no_telp'),
            $this->put('pesan'),
            $this->put('jadwal'),
            $this->put('status'),
            $this->put('created_date'),
            $this->put('created_by'),
            $this->put('is_delete'),
        );
        $this->response($response);            
    }
    // hapus data pesan_jadwal
    public function delete_delete()
    {
        $response = $this->M_pesan_jadwal_api->delete_pesan_jadwal(
            $this->delete('id_pesan_jadwal')
        );
        $this->response($response);
    }

}

?>