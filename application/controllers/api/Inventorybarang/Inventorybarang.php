<?php
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Inventorybarang extends REST_Controller

{
    public function __construct()
    {
        parent:: __construct();
        $this->load->database();
        $this->load->model('M_inventorybarang/M_inventorybarang', 'inventory_barang');
    }

    public function index_get()
    {   
        // $param['select'] = "nama_barang,status,keterangan";
        $param['where'] = array(
            'nama_barang' => "Lenovo-T420",
            'status' => "Y",
        );
        $data = $this->inventory_barang->get_all(null,null,'id_barang','DESC',$param);
        $this->response($data, REST_Controller::HTTP_OK);
    }

    public function ajax_datatable_post(){
        $row = $this->inventory_barang->get_datatables();
        $data = array_filter($row, function($v) {
            return $v;
        });

        $response = array(
            "draw"            => $this->input->post('draw'),
            "recordsTotal"    => $this->client_model->count_all(),
            "recordsFiltered" => $this->client_model->count_filtered(),
            "data"            => $data,
        );

        $this->response($response);
    }

    public function add_post()
    {

        // input post  data 
        $id_users        = $this->post('id_users');
        $nama_barang     = $this->post('nama_barang');
        $type_barang     = $this->post('type_barang');
        $qty             = $this->post('qty');
        $keterangan      = $this->post('keterangan');
        $password_laptop = $this->post('password_laptop');
        $status          = $this->post('status');
        $created_by      = $this->post('created_by');
        $created_date    = date('Y-m-d H:i:s');
        $is_delete       = $this->post('is_delete');

        $data = array(
            'id_users'        => $id_users,
            'nama_barang'     => $nama_barang,
            'type_barang'     => $type_barang,
            'keterangan'      => $keterangan,
            'qty'             => $qty,
            'status'          => $status,
            'password_laptop' => $password_laptop,
            'created_by'      => $created_by,
            'created_date'    => $created_date,
            'is_delete'       => $is_delete

        );
        $insert = $this->model_inventory->insert($data);
        if (!$insert) {
            $response['status']  = 500;
            $response['error']   = true;
            $response['message'] = 'data gagal di input';
            $this->response($response, 400);
        } else {
            $this->response('data berhasil di input', 200);
        }
    }

    public function update_post()
    {
        // input post  data 
        $id_barang       = $this->post('id_barang');
        $id_users        = $this->post('id_users');
        $nama_barang     = $this->post('nama_barang');
        $type_barang     = $this->post('type_barang');
        $keterangan      = $this->post('keterangan');
        $qty             = $this->post('qty');
        $status          = $this->post('status');
        $password_laptop = $this->post('password_laptop');
        $created_by      = $this->post('created_by');
        $created_date    = $this->post('created_date');
        $update_by       = $this->post('update_by');
        $update_date     = date('Y-m-d H:i:s');
        $is_delete       = $this->post('is_delete');

        $data = array(
            'id_users'        => $id_users,
            'nama_barang'     => $nama_barang,
            'type_barang'     => $type_barang,
            'keterangan'      => $keterangan,
            'qty'             => $qty,
            'status'          => $status,
            'password_laptop' => $password_laptop,
            'created_by'      => $created_by,
            'created_date'    => $created_date,
            'update_by'       => $update_by,
            'update_date'     => $update_date,
            'is_delete'       => $is_delete

        );



        $update = $this->model_inventory->update($id_barang,$data);

        if (!$update) {

            $response['status']  = 500;
            $response['error']   = true;
            $response['message'] = 'data gagal di input';
            $this->response($response, 400);
        } else {
            $this->response('data berhasil di input', 200);
        }
    }

    public function delete_post()
    {
        $id_barang = $this->post('id_barang');
        $delete    = $this->model_inventory->delete($id_barang);
        if ($delete) {
            $response['status']  = 200;
            $response['error']   = false;
            $response['message'] = 'Data user dihapus.';
            return $this->response($response);
        } else {
            $response['status']  = 400;
            $response['error']   = true;
            $response['message'] = 'Data user gagal dihapus.';
            return $this->response($response);
        }
    }
    
}


// response code
// 200 / 201 => success
// 404 => Not Found
// 500 => Internal Error
// 400 => Bad Request
// Get All
// Delete