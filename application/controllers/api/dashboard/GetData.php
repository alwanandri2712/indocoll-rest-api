<?php 

// defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class GetData extends REST_Controller
{
    
	public function __construct()
	{
		header('Access-Control-Allow-Origin: *');
	    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
        // $this->load->database('history_get',true);
        $this->load->model('GetData/M_GetData');
	}

	public function index_post()
	{
		$client = $this->input->get('client');
		$month  = $this->input->get('month') ?? date('m');
		$result = $this->M_GetData->get_datatables($client, $month);
		$data   = array();
		$no     = 1 + $this->input->post('start');
		foreach ($result as $key) {
			array_push($data, array(
				'no'             => $no++,
				'id_history_get' => $key->id_history_get,
				'fk_id_user'     => $key->fk_id_user,
				'nm_client'      => $key->nm_client,
				'nm_file'        => $key->nm_file,
				'ttl_hits'       => $key->ttl_hits,
				'created_date'   => $key->created_date,
				'created_by'     => $key->created_by
			));
		}

		$output = array(
				"draw"            => $this->input->post('draw'),
				"recordsTotal"    => $this->M_GetData->count_all($client, $month),
				"recordsFiltered" => $this->M_GetData->count_filtered($client, $month),
				"data"            => $data,
		);
		//output to json format
		$this->response($output, 201);
	}

	public function dashboard_get()
	{
	    $date              = $this->input->get('date') ?? date('Y-m-d');
	    $month             = $this->input->get('month') ?? date('m');
	    $client            = $this->input->get('client');
	    $result            = $this->M_GetData->get_all_get_data($client, $month);
	    $saldo_server      = $this->M_GetData->get_settings();
	    $ttl_hits_today    = $this->M_GetData->get_ttl_hits_today($client, $date);
	    $harga_per_request = $this->M_GetData->get_settings_where('harga_per_request');
	    $ttl_hits          = array();
        // print_r($result); die;
		foreach ($result as $key) {
			$ttl_hits[] = $key->ttl_hits;
		}
            
        $kuota_request  = $saldo_server[0]->value - array_sum($ttl_hits);
        
		$sisa_saldo     = $saldo_server[0]->value * $harga_per_request[0]->value;
		$ttl_saldo_used = array_sum($ttl_hits) * $harga_per_request[0]->value;

		$response = array(
			"client"         => $client,
			"kuota_request"  => number_format($saldo_server[0]->value),
			'ttl_hits'       => number_format(array_sum($ttl_hits)),
			"ttl_hits_today" => number_format($ttl_hits_today[0]->ttl_hits),
			'download_excel' => count($result),
			'sisa_saldo'     => "Rp. ".number_format($sisa_saldo - $ttl_saldo_used),
			"ttl_saldo_used" => "Rp. ".number_format($ttl_saldo_used),

		);

		$this->response($response, 201);
	}

}
