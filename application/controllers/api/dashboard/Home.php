<?php

// defined('BASEPATH') or exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
header('Access-Control-Allow-Origin: *');

use Restserver\Libraries\REST_Controller;
class Home extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('sms/M_sms', 'history_sms_model');
        $this->db                = $this->load->database('pt_indocoll',true);
        $this->db2               = $this->load->database('get_data', true);
    }

    public function index_get($params = null)
    {
        if ($params == 'card-dashboard') {
            $tanggal            = date("Y-m-d");
            $target_daily       = 87945269;
            $target_weekly      = 615616855;
            $target_monthly     = 2286577000;
            $payment_today      = $this->db->query("SELECT sum(penerimaan) AS ttl_penerimaan FROM `keuangan` WHERE is_delete = 0 AND tanggal_penerimaan = '$tanggal' ")->result();
            $persentase_today   = round($payment_today[0]->ttl_penerimaan / $target_daily * 100, 2);
            $week_start         = date("Y-m-d", strtotime($tanggal));
            $week_end           = date("Y-m-d", strtotime("-1 week", strtotime($tanggal)));
            $payment_weekly     = $this->db->query("SELECT sum(penerimaan) AS ttl_penerimaan_week FROM `keuangan` WHERE is_delete = 0 AND tanggal_penerimaan BETWEEN '$week_end' AND '$week_start' ")->result();
            $persentase_week    = round($payment_weekly[0]->ttl_penerimaan_week / $target_weekly * 100, 2);
            $payment_monthly    = $this->db->query("SELECT sum(penerimaan) AS ttl_penerimaan_monthly FROM `keuangan` WHERE is_delete = 0 AND MONTH(tanggal_penerimaan) = MONTH(CURDATE()) ")->result();
            $persentase_monthly = round($payment_monthly[0]->ttl_penerimaan_monthly / $target_monthly * 100, 2);
            $total_payment      = $this->db->query("SELECT sum(penerimaan) AS ttl_penerimaan FROM keuangan WHERE is_delete = 0 ")->result();
            $total_pengeluaran  = $this->db->query("SELECT sum(total) AS ttl_pengeluaran FROM pengeluaran ")->result();
            $ttl_user           = $this->db->query("SELECT * FROM user WHERE is_delete = 0")->num_rows();

            if ((float) $persentase_today > 50) {
                $percentage_daily_html = '<p class="text-success">= <i class="fa fa-arrow-up"></i> ' . $persentase_today . '%</p>';
            } else {
                $percentage_daily_html = '<p class="text-danger">= <i class="fa fa-arrow-down"></i> ' . $persentase_today . '%</p>';
            }

            if ((float) $persentase_week > 50) {
                $percentage_week_html = '<p class="text-success">= <i class="fa fa-arrow-up"></i> ' . $persentase_week . '%</p>';
            } else {
                $percentage_week_html = '<p class="text-danger">= <i class="fa fa-arrow-down"></i> ' . $persentase_week . '%</p>';
            }

            try {
                $response = [
                    'message' => [
                        'payment_daily' => [
                            'data_daily'   => "Rp. " . number_format($payment_today[0]->ttl_penerimaan, 2, ".", "."),
                            'target_daily' => "Rp. " . number_format($target_daily, 2, ".", "."),
                            'percentage'   => $percentage_daily_html,
                            'date'         => $tanggal
                        ],
                        'payment_week' => [
                            'data_week'   => "Rp. " . number_format($payment_weekly[0]->ttl_penerimaan_week, 2, ".", "."),
                            'target_week' => "Rp. " . number_format($target_weekly, 2, ".", "."),
                            'percentage'  => $percentage_week_html,
                            'date'        => $week_start . ' - ' . $week_end
                        ],
                        'payment_monthly' => [
                            'data_monthly'   => "Rp. " . number_format($payment_monthly[0]->ttl_penerimaan_monthly, 2, ".", "."),
                            'target_monthly' => "Rp. " . number_format($target_monthly, 2, ".", "."),
                            'percentage'     => "= " . $persentase_monthly . "%",
                        ],
                        'total_payment'     => "Rp. " . number_format($total_payment[0]->ttl_penerimaan, 2, ".", "."),
                        'total_pengeluaran' => "Rp. " . number_format($total_pengeluaran[0]->ttl_pengeluaran, 2, ".", "."),
                        'total_karyawan'    => $ttl_user,
                    ]
                ];
            } catch (\Throwable $th) {
                $response = [];
            }

            $this->response($response, REST_Controller::HTTP_OK);
        } elseif ($params == 'sms-dashboard') {
            $this->load->library('sms');
            $username           = USERNAME_SMS;
            $password           = PASSWORD_SMS;
            $url                = "http://sms.mysmsmasking.com/masking/balance.php?username=$username&password=$password";
            $total_saldo_sms    = $this->sms->saldoSMS($url);
            $parsingBlok = strtolower(substr($total_saldo_sms, 13, 5));
            if ($parsingBlok == 'block') {
                $response = [
                    'message' => [
                        'code' => "500",
                    '   status' => false,
                        'data_sms' => "User Anda Di Block"
                    ]
                ];
                $this->response($response, REST_Controller::HTTP_OK);
            } else {

                if ($total_saldo_sms != false) {
                    $parse_saldo = number_format(substr($total_saldo_sms, 0, -21), 2, ",", ",");
                } else {
                    $parse_saldo = 0;
                }
                
                $expired_expl = explode(",", $total_saldo_sms);
    
                try {
                    $response = [
                        'message' => [
                            'data_sms' => "Rp. " . $parse_saldo,
                            'expired' => $expired_expl[1],
                        ]
                    ];
                } catch (\Throwable $th) {
                    $response = [];
                }
                $this->response($response, REST_Controller::HTTP_OK);
            }

        }
    }

    public function grafikpaymentdaily_post()
    {
        $kalender = CAL_GREGORIAN;
        $bulan    = isset($_POST['bulan']) ? $_POST['bulan'] : date('m');
        $tahun    = isset($_POST['tahun']) ? $_POST['tahun'] : date('Y');
        $hari     = cal_days_in_month($kalender, $bulan, $tahun);

        $label_daily = [];

        for ($i = 1; $i < $hari + 1; $i++) {
            $format_date_daily = date_create($_POST['tahun'] . '-' . $_POST['bulan'] . '-' . $i);
            array_push($label_daily, isset($_POST['bulan']) ? date_format($format_date_daily, "Y/m/d") : date("Y-m-") . $i);
        }

        for ($daily = 1; $daily < $hari + 1; $daily++) {
            if (isset($_POST['bulan'])) {
                $tahun_post = $_POST['tahun'];
                $bulan_post = $_POST['bulan'];
                $ttl_date   = $tahun_post . '-' . $bulan_post . '-' . $daily;
                $grafik_dc  = $this->db->query("SELECT sum(penerimaan) as ttl_dc FROM keuangan WHERE is_delete = 0 AND DATE(tanggal_penerimaan) = '$ttl_date' AND MONTH(tanggal_penerimaan) = '$bulan_post' AND YEAR(tanggal_penerimaan) = '$tahun_post'")->result();
            } else {
                $ttl_date  = date("Y-m-") . $daily;
                $grafik_dc = $this->db->query("SELECT sum(penerimaan) as ttl_dc FROM keuangan WHERE is_delete = 0 AND DATE(tanggal_penerimaan) = '$ttl_date'")->result();
            }

            $ttl_dc[] = !empty($grafik_dc[0]->ttl_dc) ? $grafik_dc[0]->ttl_dc : 0;
        }

        try {
            $response = array(
                'label' => $label_daily,
                'data'  => $ttl_dc
            );
        } catch (\Throwable $th) {
            $response = [];
        }

        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function getdatatraffic_post()
    {
        $kalender = CAL_GREGORIAN;
        $bulan    = isset($_POST['bulan']) ? $_POST['bulan'] : date('m');
        $tahun    = isset($_POST['tahun']) ? $_POST['tahun'] : date('Y');
        $hari     = cal_days_in_month($kalender, $bulan, $tahun);

        $label_daily = [];

        for ($i = 1; $i < $hari + 1; $i++) {
            $format_date_daily = date_create($tahun . '-' . $bulan . '-' . $i);
            array_push($label_daily, isset($_POST['bulan']) ? date_format($format_date_daily, "Y/m/d") : date("Y-m-") . $i);
        }

        for ($daily = 1; $daily < $hari + 1; $daily++) {
            if (isset($_POST['bulan'])) {
                $tahun_post = $tahun;
                $bulan_post = $bulan;
                $ttl_date   = $tahun_post . '-' . $bulan_post . '-' . $daily;
                $grafik_dc  = $this->db2->query("SELECT SUM(ttl_hits) as ttl_hits FROM history_get WHERE DATE(created_date) = '$ttl_date' AND MONTH(created_date) = '$bulan_post' AND YEAR(created_date) = '$tahun_post'")->result();
            } else {
                $ttl_date  = date("Y-m-") . $daily;
                $grafik_dc = $this->db2->query("SELECT SUM(ttl_hits) as ttl_hits FROM history_get WHERE DATE(created_date) = '$ttl_date'")->result();
            }

            $ttl_hits[] = !empty($grafik_dc[0]->ttl_hits) ? $grafik_dc[0]->ttl_hits : 0;
        }

        try {
            $response = array(
                'label' => $label_daily,
                'data'  => $ttl_hits
            );
        } catch (\Throwable $th) {
            $response = [];
        }

        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function smstraffic_post(){
        $kalender = CAL_GREGORIAN;
        $bulan    = isset($_POST['bulan']) ? $_POST['bulan'] : date('m');
        $tahun    = isset($_POST['tahun']) ? $_POST['tahun'] : date('Y');
        $hari     = cal_days_in_month($kalender, $bulan, $tahun);

        $label_daily = [];

        for ($i = 1; $i < $hari + 1; $i++) {
            $format_date_daily = date_create($tahun . '-' . $bulan . '-' . $i);
            array_push($label_daily, isset($_POST['bulan']) ? date_format($format_date_daily, "Y/m/d") : date("Y-m-") . $i);
        }

        for ($daily = 1; $daily < $hari + 1; $daily++) {
            if (isset($_POST['bulan'])) {
                $tahun_post = $tahun;
                $bulan_post = $bulan;
                $ttl_date   = $tahun_post . '-' . $bulan_post . '-' . $daily;
                $grafik_dc  = $this->db->query("SELECT COUNT(*) as ttl_hits FROM history_sms WHERE DATE(created_date) = '$ttl_date' AND MONTH(created_date) = '$bulan_post' AND YEAR(created_date) = '$tahun_post'")->result();
            } else {
                $ttl_date  = date("Y-m-") . $daily;
                $grafik_dc = $this->db->query("SELECT COUNT(*) as ttl_hits FROM history_sms WHERE DATE(created_date) = '$ttl_date'")->result();
            }

            $ttl_hits[] = !empty($grafik_dc[0]->ttl_hits) ? $grafik_dc[0]->ttl_hits : 0;
        }

        try {
            $response = array(
                'label' => $label_daily,
                'data'  => $ttl_hits
            );
        } catch (\Throwable $th) {
            $response = [];
        }

        $this->response($response, REST_Controller::HTTP_OK);
    }

    public function datatableSMS_post(){

        $month      = isset($_POST['bulan']) ? $_POST['bulan'] : date('m');
        $year       = isset($_POST['tahun']) ? $_POST['tahun'] : date('Y');
        
        $result = $this->history_sms_model->get_datatables($month, $year);

        $data = array_filter($result, function ($v) {
            $no = $this->input->post('start');
            $v->no = $no++;
            return $v;
        });

        $output = array(
            "draw" => $this->input->post('draw'),
            "recordsTotal" => $this->history_sms_model->count_all($month, $year),
            "recordsFiltered" => $this->history_sms_model->count_filtered($month, $year),
            "data" => $data,
        );

        $this->response($output, REST_Controller::HTTP_OK);

    }
}
        
    /* End of file  Home.php */
