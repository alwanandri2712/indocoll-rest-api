<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;
class Welcome extends REST_Controller {

	public function index_get()
	{	
		$developer = array(
			array(
				'name' => 'Alwan',
				'role' => 'Manager IT',
			),
			array(
				'name' => 'Iqbal',
				'role' => 'Staff IT',
			),
			array(
				'name' => 'Agung Ferianto',
				'role' => 'Staff IT',
			),
			array(
				'name' => 'Zidan',
				'role' => 'PKL',
			),
		);

		$response = [
			'code' => true,
			'message' => "ini api indocoll",
			'developer' => $developer
		];
		$this->response($response);
	}
}
