<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('response_json')) {
    function response_json($code = 200, $error = false, $message = 'success', $data = array() )
    {
        $response = array
        (
            'code'      => $code,
            'error'     => $error,
            'messgae'   => $message,
            'data'      => $data,

        );
        
        return $response;
    }
}

if (!function_exists('groupByObject')) {
    function groupByObject($array, $key)
    {
        $return = array();
        foreach ($array as $val) {
            $return[$val->{$key}][] = $val;
        }
        return $return;
    }
}

if (!function_exists('random_string_idcl')) {
    function random_string_idcl($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= strtoupper($keys[array_rand($keys)]);
        }

        return $key;
    }
}

if (!function_exists('phone_hp')) {
    function phone_hp($nohp = null) {
         $hp = "";
         // kadang ada penulisan no hp 0811 239 345
         $nohp = str_replace(" ","",$nohp);
         // kadang ada penulisan no hp (0274) 778787
         $nohp = str_replace("(","",$nohp);
         // kadang ada penulisan no hp (0274) 778787
         $nohp = str_replace(")","",$nohp);
         // kadang ada penulisan no hp 0811.239.345
         $nohp = str_replace(".","",$nohp);

         // cek apakah no hp mengandung karakter + dan 0-9
         if(!preg_match('/[^+0-9]/',trim($nohp))){
             // cek apakah no hp karakter 1-3 adalah +62
             if(substr(trim($nohp), 0, 3)=='+62'){
                 $hp .= trim($nohp);
             }
             // cek apakah no hp karakter 1 adalah 0
             elseif(substr(trim($nohp), 0, 1) == '8'){
                 $hp .= '0'.$nohp;
             }else{
                $hp .= $nohp;
             }
         }
         return $hp;
     }
}

if (!function_exists('safe_json_encode')) {
    function safe_json_encode($value)
    {
        if (version_compare(PHP_VERSION, '5.4.0') >= 0) {
            $encoded = json_encode($value, JSON_PRETTY_PRINT);
        } else {
            $encoded = json_encode($value);
        }
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return $encoded;
            case JSON_ERROR_DEPTH:
                return 'Maximum stack depth exceeded'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_STATE_MISMATCH:
                return 'Underflow or the modes mismatch'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_CTRL_CHAR:
                return 'Unexpected control character found';
            case JSON_ERROR_SYNTAX:
                return 'Syntax error, malformed JSON'; // or trigger_error() or throw new Exception()
            case JSON_ERROR_UTF8:
                $clean = utf8ize($value);
                return safe_json_encode($clean);
            default:
                return 'Unknown error'; // or trigger_error() or throw new Exception()

        }
    }
}

if (!function_exists('utf8ize')) {
    function utf8ize($mixed)
    {
        if (is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = utf8ize($value);
            }
        } else if (is_string($mixed)) {
            return utf8_encode($mixed);
        }
        return $mixed;
    }
}

if (!function_exists('getallheaders')) {
    function getallheaders()
    {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}

if (!function_exists('valid_image')) {
    function valid_image($url)
    {
        $headers = @get_headers(@$url, 1);
        if (strpos(@$headers['Content-Type'], 'image/') !== false) {
            return true;
        } else {
            return false;
        }
    }
}

if (!function_exists('curl_get')) {
    function curl_get($url, $headers = "")
    {
        $ch = curl_init();
        $options = [
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_URL            => $url,
            CURLOPT_TIMEOUT        => 0,
        ];

        if ($headers) {
            $options[CURLOPT_HTTPHEADER] =  $headers;
        }

        curl_setopt_array($ch, $options);
        $data = json_decode(curl_exec($ch));
        curl_close($ch);
        return $data;
    }
}

if (!function_exists('curl_post')) {
    function curl_post($url, $fields, $headers = "")
    {
        $options = array(
            'http' => array(
                'method'  => 'POST',
                'content' => json_encode($fields),
                'header' =>  $headers
            )
        );

        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        return $result;
    }
}

if (!function_exists('curlPostJson')) {
    function curlPostJson($url, $data = array(), $headers = [])
    {
        // Create a new cURL resource
        $ch = curl_init($url);

        // Setup request to send json via POST
        $payload = json_encode($data);

        // Attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set the content type to application/json
        if (!$headers) {
            $headers[] = 'Content-Type: application/json';
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $rest = json_decode(curl_exec($ch));
        curl_close($ch);
        return $rest;
    }
}

if (!function_exists('curlPost')) {
    function curlPost($url, $data = array(), $headers = "")
    {
        $cURLConnection = curl_init($url);
        curl_setopt($cURLConnection, CURLOPT_POSTFIELDS, $data);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        // Set the content type to application/json
        if ($headers) {
            curl_setopt($cURLConnection, CURLOPT_HTTPHEADER, $headers);
        }

        $apiResponse = curl_exec($cURLConnection);
        $jsonArrayResponse = json_decode($apiResponse);
        curl_close($cURLConnection);
        // $apiResponse - available data from the API request
        return $jsonArrayResponse;
    }
}

if (!function_exists('dd')) {
    function dd($data)
    {
        header('Content-Type: application/json');
        echo json_encode($data, JSON_PRETTY_PRINT);
        die;
    }
}

if (!function_exists('responseJson')) {
    function responseJson($response, $code)
    {
        header('Content-Type: application/json');
        http_response_code($code);
        echo json_encode($response);
        die;
    }
}

if (!function_exists('toJson')) {
    function toJson($response, $code)
    {
        header('Content-Type: application/json');
        http_response_code($code);
        echo json_encode($response);
        die;
    }
}

/**
 * Calculates the great-circle distance between two points, with
 * the Haversine formula.
 * @param float $latitudeFrom Latitude of start point in [deg decimal]
 * @param float $longitudeFrom Longitude of start point in [deg decimal]
 * @param float $latitudeTo Latitude of target point in [deg decimal]
 * @param float $longitudeTo Longitude of target point in [deg decimal]
 * @param float $earthRadius Mean earth radius in [m]
 * @return float Distance between points in [m] (same as earthRadius)
 */
if (!function_exists('haversineGreatCircleDistance')) {
    function haversineGreatCircleDistance(
        $latitudeFrom,
        $longitudeFrom,
        $latitudeTo,
        $longitudeTo,
        $earthRadius = 6371000
    ) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return floor($angle * $earthRadius);
    }
}


if (!function_exists('requestJson')) {
    function requestJson()
    {
        $ci = &get_instance();
        $stream_clean = $ci->security->xss_clean($ci->input->raw_input_stream);
        $request = json_decode($stream_clean);
        return $request;
    }
}

if (!function_exists('requestGet')) {
    function requestGet($url)
    {
        $parts = parse_url($url);
        $fp = fsockopen($parts['host'], isset($parts['port']) ? $parts['port'] : 80, $errno, $errstr, 30);
        $out = "GET " . $parts['path'] . " HTTP/1.1\r\n";
        $out .= "Host: " . $parts['host'] . "\r\n";
        $out .= "Content-Length: 0" . "\r\n";
        $out .= "Connection: Close\r\n\r\n";

        fwrite($fp, $out);
        fclose($fp);
    }
}

if (!function_exists('curlPostJson')) {
    function curlPostJson($url, $data = array(), $headers = [])
    {
        // Create a new cURL resource
        $ch = curl_init($url);

        // Setup request to send json via POST
        $payload = json_encode($data);

        // Attach encoded JSON string to the POST fields
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set the content type to application/json
        if (!$headers) {
            $headers[] = 'Content-Type: application/json';
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $rest = json_decode(curl_exec($ch));
        curl_close($ch);
        return $rest;
    }
}

if (!function_exists('get_image_mime_type')) {
    function get_image_mime_type($image_path)
    {
        $mimes  = array(
            IMAGETYPE_GIF => "image/gif",
            IMAGETYPE_JPEG => "image/jpg",
            IMAGETYPE_PNG => "image/png",
            IMAGETYPE_SWF => "image/swf",
            IMAGETYPE_PSD => "image/psd",
            IMAGETYPE_BMP => "image/bmp",
            IMAGETYPE_TIFF_II => "image/tiff",
            IMAGETYPE_TIFF_MM => "image/tiff",
            IMAGETYPE_JPC => "image/jpc",
            IMAGETYPE_JP2 => "image/jp2",
            IMAGETYPE_JPX => "image/jpx",
            IMAGETYPE_JB2 => "image/jb2",
            IMAGETYPE_SWC => "image/swc",
            IMAGETYPE_IFF => "image/iff",
            IMAGETYPE_WBMP => "image/wbmp",
            IMAGETYPE_XBM => "image/xbm",
            IMAGETYPE_ICO => "image/ico"
        );

        if (($image_type = exif_imagetype($image_path))
            && (array_key_exists($image_type, $mimes))
        ) {
            return $mimes[$image_type];
        } else {
            return FALSE;
        }
    }
}


if (!function_exists('slaap')) {
    function slaap($seconds)
    {
        $seconds = abs($seconds);
        if ($seconds < 1) :
            usleep($seconds * 1000000);
        else :
            sleep($seconds);
        endif;
    }
}

/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
/*::                                                                         :*/
/*::  This routine calculates the distance between two points (given the     :*/
/*::  latitude/longitude of those points). It is being used to calculate     :*/
/*::  the distance between two locations using GeoDataSource(TM) Products    :*/
/*::                                                                         :*/
/*::  Definitions:                                                           :*/
/*::    South latitudes are negative, east longitudes are positive           :*/
/*::                                                                         :*/
/*::  Passed to function:                                                    :*/
/*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
/*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
/*::    unit = the unit you desire for results                               :*/
/*::           where: 'M' is statute miles (default)                         :*/
/*::                  'K' is kilometers                                      :*/
/*::                  'N' is nautical miles                                  :*/
/*::  Worldwide cities and other features databases with latitude longitude  :*/
/*::  are available at https://www.geodatasource.com                          :*/
/*::                                                                         :*/
/*::  For enquiries, please contact sales@geodatasource.com                  :*/
/*::                                                                         :*/
/*::  Official Web site: https://www.geodatasource.com                        :*/
/*::                                                                         :*/
/*::         GeoDataSource.com (C) All Rights Reserved 2018                  :*/
/*::                                                                         :*/
/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

if (!function_exists('distance')) {
    function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return round(($miles * 1.609344), 1);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }
}


if (!function_exists('array_column_portable')) {
    function array_column_portable($array, $key)
    {
        return array_map(function ($e) use ($key) {
            return is_object($e) ? $e->$key : $e[$key];
        }, $array);
    }
}

if (!function_exists('getElastic')) {
    function getElastic($config, $body, $object = FALSE)
    {
        $curl = curl_init();

        $host = $config['host'];
        $index = $config['index'];
        $type = !empty($config['type']) ? $config['type'] . '/' :  "";

        $url = $host . '/' . $index . '/' . $type . '_search';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_ENCODING => '',
            // CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 5,
            // CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        if ($object) {
            return json_decode($response);
        } else {
            return json_decode($response, true);
        }
    }
}

if (!function_exists('updateElastic')) {
    function updateElastic($config, $indexingId, $body)
    {
        $curl = curl_init();

        $host = $config['host'];
        $index = $config['index'];
        $type = !empty($config['type']) ? $config['type'] . '/' :  "";

        $url = $host . '/' . $index . '/' . $type . '_update/' . $indexingId;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_ENCODING => '',
            // CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 5,
            // CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        curl_exec($curl);
        curl_close($curl);
    }
}

if (!function_exists('insertElastic')) {
    function insertElastic($config, $indexingId, $body)
    {
        $curl = curl_init();

        $host = $config['host'];
        $index = $config['index'];
        $type = !empty($config['type']) ? $config['type'] . '/' :  "";

        $url = $host . '/' . $index . '/' . $type . $indexingId;

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_ENCODING => '',
            // CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 5,
            // CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        curl_exec($curl);
        curl_close($curl);
    }
}

if (!function_exists('deleteQueryElastic')) {
    function deleteQueryElastic($config, $body)
    {
        $curl = curl_init();

        $host = $config['host'];
        $index = $config['index'];
        $type = !empty($config['type']) ? $config['type'] . '/' :  "";

        $url = $host . '/' . $index . '/_delete_by_query';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_ENCODING => '',
            // CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 5,
            // CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        curl_exec($curl);
        curl_close($curl);
    }
}

if (!function_exists('getElasticCount')) {
    function getElasticCount($config, $body, $object = FALSE)
    {
        $curl = curl_init();

        $host = $config['host'];
        $index = $config['index'];
        $type = !empty($config['type']) ? $config['type'] . '/' :  "";

        $url = $host . '/' . $index . '/' . $type . '_count';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_ENCODING => '',
            // CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 5,
            // CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        if ($object) {
            return json_decode($response);
        } else {
            return json_decode($response, true);
        }
    }
}

if (!function_exists('debugJson')) {
    function debugJson()
    {
        header('Content-Type: application/json');
        $params = func_get_args();
        $count = count($params);
        if ($count == 1) {
            echo json_encode($params[0], JSON_PRETTY_PRINT);
        } else {
            echo json_encode($params, JSON_PRETTY_PRINT);
        }
        die;
    }
}

if (!function_exists('dump')) {
    function dump()
    {
        echo "<pre>";
        $params = func_get_args();
        $count = count($params);
        if ($count == 1) {
            var_dump($params[0]);
        } else {
            var_dump($params);
        }
        die;
    }
}